# SphereDetectionModel
Sphere Detection Mask R-CNN Model


## Citation

If you use this project for a publication, please cite the [paper](https://hal.science/hal-04160733):
  ```
  @inproceedings{fainsin2023neural,
    title={Neural detection of spheres in images for lighting calibration},
    author={Fainsin, Laurent and M{\'e}lou, Jean and Calvet, Lilian and Carlier, Axel and Durou, Jean-Denis},
    booktitle={Sixteenth International Conference on Quality Control by Artificial Vision},
    volume={12749},
    pages={305--311},
    year={2023},
    organization={SPIE}
  }
  ```
